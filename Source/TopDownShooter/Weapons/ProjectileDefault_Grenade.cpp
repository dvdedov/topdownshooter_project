// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugExplosionShow = 0;
FAutoConsoleVariableRef VARExplosionShow(
	TEXT("TopDownShooter.DebugExplosion"),
	DebugExplosionShow,
	TEXT("Draw Debug for Explosde"),
	ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		TimerExplosion(DeltaTime);
	}
}

void AProjectileDefault_Grenade::TimerExplosion(float DeltaTime)
{
	if(TimerEnabled)
	{

		if (TimerToExplosion > TimeToExplosion)
		{
			//Explosion
			Explosion_OnServer();
		}
		else
		{
			TimerToExplosion += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explosion_OnServer();
	}

	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//InitGrenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explosion_OnServer_Implementation()
{
	if (DebugExplosionShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Red, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage + (ProjectileSetting.ProjectileMaxRadiusDamage - ProjectileSetting.ProjectileMinRadiusDamage) / 2, 12, FColor::Orange, false, 12.0f);
	}

	TimerEnabled = false;
	if (ProjectileSetting.ExplosionFX)
	{
		//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExplosionFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
		SpawnExplosionFX_Multicast(ProjectileSetting.ExplosionFX);
	}
	if (ProjectileSetting.ExplosionSound)
	{
		//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplosionSound, GetActorLocation());
		SpawnExplosionSound_Multicast(ProjectileSetting.ExplosionSound);
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplosionMaxDamage,
		ProjectileSetting.ExplosionMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		ProjectileSetting.ExplosionFalloffCoef,
		NULL, IgnoredActor, this, GetInstigatorController());

	this->Destroy();
}

void AProjectileDefault_Grenade::SpawnExplosionFX_Multicast_Implementation(UParticleSystem* FXTemplate)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FXTemplate, GetActorLocation(), GetActorRotation(), FVector(1.0f));

}

void AProjectileDefault_Grenade::SpawnExplosionSound_Multicast_Implementation(USoundBase* ExplosionSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSound, GetActorLocation());

}
