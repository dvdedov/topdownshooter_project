// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TopDownShooter/Weapons/ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	//Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	//Called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplosion(float DeltaTime);

	void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	UFUNCTION(NetMulticast, Reliable)
	void Explosion_OnServer();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	bool TimerEnabled = false;
	float TimerToExplosion = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	float TimeToExplosion = 5.0f;

	UFUNCTION(NetMulticast, Reliable)
		void SpawnExplosionFX_Multicast(UParticleSystem* FXTemplate);
	UFUNCTION(NetMulticast, Reliable)
		void SpawnExplosionSound_Multicast(USoundBase* ExplosionSound);
};
