// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownShooter/StateEffects/TPS_StateEffect.h"
#include "TopDownShooter/Character/TPSHealthComponent.h"
#include "TopDownShooter/Interface/TPS_IGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

bool UTPS_StateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;
	NameBone = NameBoneHit;

	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_AddEffect(myActor, this);
	}

	//if (ParticleEffect)
	//{
	//	FName NameBoneToAttach= NameBoneHit;
	//	FVector Loc = FVector(0);

	//	USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	//	if (myMesh)
	//	{
	//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, NameBoneToAttach, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	//	}
	//	else
	//	{
	//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttach, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	//	}
	//}
	return true;
}

void UTPS_StateEffect::DestroyObject()
{
	//if (ParticleEmitter)
	//{
	//	ParticleEmitter->DestroyComponent();
	//	ParticleEmitter = nullptr;
	//}

	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_RemoveEffect(myActor, this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UTPS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComponent = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			myHealthComponent->ChangeHealthValue_OnServer(Power);
		}
	}

	DestroyObject();
}

bool UTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTPS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTPS_StateEffect_ExecuteTimer::Execute, RateTimer, bIsTicking);
	}
	
	return true;
}

void UTPS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComponent = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			myHealthComponent->ChangeHealthValue_OnServer(Power);
		}
	}
}

bool UTPS_StateEffect_TempInvincible::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	Execute();
	return true;
}

void UTPS_StateEffect_TempInvincible::DestroyObject()
{
	Super::DestroyObject();
}

void UTPS_StateEffect_TempInvincible::Execute()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComponent = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			if (myHealthComponent->CoefDamage > TempCoefDamage)
				{
					myHealthComponent->CoefDamage = TempCoefDamage;
				}
			else
				myHealthComponent->CoefDamage = 1.0f;
		}
	}
}

void UTPS_StateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPS_StateEffect, NameBone);

}