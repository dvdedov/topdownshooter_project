// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "TPS_StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TOPDOWNSHOOTER_API UTPS_StateEffect : public UObject
{
	GENERATED_BODY()

public:
	bool IsSupportedForNetworking() const override { return true; };
	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStackable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsAutoDestroyParticleEffect = false;

	AActor* myActor = nullptr;
	UPROPERTY(Replicated)
	FName NameBone;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	UParticleSystem* ParticleEffect = nullptr;

	//UParticleSystemComponent* ParticleEmitter = nullptr;
};

UCLASS()
class TOPDOWNSHOOTER_API UTPS_StateEffect_ExecuteOnce : public UTPS_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;
};

UCLASS()
class TOPDOWNSHOOTER_API UTPS_StateEffect_ExecuteTimer : public UTPS_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Acto, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		bool bIsTicking = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float RateTimer = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};

UCLASS()
class TOPDOWNSHOOTER_API UTPS_StateEffect_TempInvincible : public UTPS_StateEffect_ExecuteTimer
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	void Execute() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Temporary Invincible")
		float TempCoefDamage = 0.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};