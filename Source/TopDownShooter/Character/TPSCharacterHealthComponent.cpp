// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownShooter/Character/TPSCharacterHealthComponent.h"
#include "Kismet/GameplayStatics.h"

void UTPSCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(CurrentDamage);
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	//OnShieldChange.Broadcast(Shield, ChangeValue);
	ShieldChangeEvent_Multicast(Shield, ChangeValue);

	if (Shield > 0.0f && ShieldDamagedParticle)
	{
		//FX when shield get damage
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ShieldDamagedParticle, GetOwner()->GetActorLocation());
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ShieldDamagedSound, GetOwner()->GetActorLocation());
	}

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;

			if (ShieldBrokenParticle)
			{
				//FX when shield is broken
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ShieldBrokenParticle, GetOwner()->GetActorLocation());
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ShieldBrokenSound, GetOwner()->GetActorLocation());
			}
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownSheildTimer, this, &UTPSCharacterHealthComponent::CollDownShieldEnd, CollDownShieldRecoveryTime, false);
	
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
}

void UTPSCharacterHealthComponent::CollDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;

		if (ShieldRecoveredParticle)
		{
			//FX when shield is recovered
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ShieldRecoveredParticle, GetOwner()->GetActorLocation());
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ShieldRecoveredSound, GetOwner()->GetActorLocation());
		}

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	//OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
	ShieldChangeEvent_Multicast(Shield, ShieldRecoveryValue);
}

float UTPSCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

float UTPSCharacterHealthComponent::GetCurrentStamina()
{
	return Stamina;
}

void UTPSCharacterHealthComponent::ChangeStaminaValue(float ChangeValue)
{
	if (ChangeValue == 0.0f)
	{
		ChangeValue = StaminaReduceRate;
	}

	Stamina += ChangeValue;

	if (Stamina > 100.0f)
	{
		Stamina = 100.0f;
	}
	else
	{
		if (Stamina < 0.0f)
		{
			Stamina = 0.0f;
			OnStaminaEnd.Broadcast();
		}
	}
	
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownStaminaTimer, this, &UTPSCharacterHealthComponent::CollDownStaminaEnd, CollDownStaminaRecoveryTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaRecoveryRateTimer);
	}

	OnStaminaChange.Broadcast(Stamina, ChangeValue);
}

void UTPSCharacterHealthComponent::CollDownStaminaEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_StaminaRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryStamina, StaminaRecoveryRate, true);
	}
}

void UTPSCharacterHealthComponent::StartReduceStamina(bool bIsReduce)
{
	if (bIsReduce)
	{
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownStaminaTimer, this, &UTPSCharacterHealthComponent::CollDownStaminaEnd, CollDownStaminaRecoveryTime, false);

			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaRecoveryRateTimer);

			GetWorld()->GetTimerManager().SetTimer(TimerHandle_StaminaReduceRateTimer, this, &UTPSCharacterHealthComponent::ReduceStamina, StaminaReduceRate, true);
		}
	}
	else
	{
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaReduceRateTimer);
		}
	}
}

void UTPSCharacterHealthComponent::ReduceStamina()
{
	float tmp = Stamina;
	tmp = tmp - StaminaReduceValue;
	if (tmp < 0.0f)
	{
		Stamina = 0.0f;
		OnStaminaEnd.Broadcast();

		StartReduceStamina(false);
	}
	else
	{
		Stamina = tmp;
		StartReduceStamina(true);
	}

	OnStaminaChange.Broadcast(Stamina, StaminaReduceValue);
}

void UTPSCharacterHealthComponent::RecoveryStamina()
{
	float tmp = Stamina;
	tmp = tmp + StaminaRecoveryValue;
	if (tmp > 100.0f)
	{
		Stamina = 100.0f;

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaRecoveryRateTimer);
		}
	}
	else
	{
		Stamina = tmp;
	}

	OnStaminaChange.Broadcast(Stamina, StaminaRecoveryValue);

}

void UTPSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float NewShield, float ChangeValue)
{
	OnShieldChange.Broadcast(NewShield, ChangeValue);
}
