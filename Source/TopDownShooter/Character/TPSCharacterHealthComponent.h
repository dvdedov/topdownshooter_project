// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TopDownShooter/Character/TPSHealthComponent.h"
#include "TPSCharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaChange, float, Stamina, float, ChangeValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStaminaEnd);

UCLASS()
class TOPDOWNSHOOTER_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
	FOnShieldChange OnShieldChange;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		FOnStaminaChange OnStaminaChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		FOnStaminaEnd OnStaminaEnd;

	FTimerHandle TimerHandle_CollDownSheildTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	FTimerHandle TimerHandle_CollDownStaminaTimer;
	FTimerHandle TimerHandle_StaminaRecoveryRateTimer;
	FTimerHandle TimerHandle_StaminaReduceRateTimer;

protected:
	float Shield = 100.0f;

	float Stamina = 100.0f;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float CollDownShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryRate = 0.1f;

	//Shield FXs
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield|SheldFX")
	UParticleSystem* ShieldDamagedParticle = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield|SheldFX")
	UParticleSystem* ShieldBrokenParticle = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield|SheldFX")
	UParticleSystem* ShieldRecoveredParticle = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield|SheldSounds")
		USoundBase* ShieldDamagedSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield|SheldSounds")
		USoundBase* ShieldBrokenSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield|SheldSounds")
		USoundBase* ShieldRecoveredSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float CollDownStaminaRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoveryRate = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaReduceValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaReduceRate = 0.1f;

	void ChangeHealthValue_OnServer(float ChangeValue) override;

	UFUNCTION(BlueprintCallable, Category = "Shield")
	float GetCurrentShield();
	UFUNCTION(BlueprintCallable, Category = "Shield")
	void ChangeShieldValue(float ChangeValue);
	void CollDownShieldEnd();
	void RecoveryShield();
	UFUNCTION(BlueprintCallable, Category = "Shield")
	float GetShieldValue();

	UFUNCTION(BlueprintCallable, Category = "Stamina")
	float GetCurrentStamina();
	UFUNCTION(BlueprintCallable, Category = "Stamina")
	void ChangeStaminaValue(float ChangeValue);

	void CollDownStaminaEnd();
	void StartReduceStamina(bool bIsReduce);
	void ReduceStamina();
	void RecoveryStamina();

	UFUNCTION(NetMulticast, Reliable)
	void ShieldChangeEvent_Multicast(float NewShield, float ChangeValue);
};
