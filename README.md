# TopDownShooter

Defeat waves of enemies to meet the boss.
Choose between single player play or invite your friend and choose your roles in the battle.
To destroy the enemy you will have a machine gun, a shotgun, a sniper rifle and a grenade launcher and your energy shields will not allow you to die from a random mistake.

Screenshots

![Menu](https://gitlab.com/dvdedov/topdownshooter_project/-/blob/master/TDSmenu.png)

![Co-op menu](https://gitlab.com/dvdedov/topdownshooter_project/-/blob/master/TDSmenu_coop.png)

![Game Level](https://gitlab.com/dvdedov/topdownshooter_project/-/blob/master/TDSGame.png)

Developed with Unreal Engine 4
